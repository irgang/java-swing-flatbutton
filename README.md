java_swing_FlatButton
=====================

Flat buttons and a flat button group for Java Swing.

![screenshot](FlatButtons.png)
