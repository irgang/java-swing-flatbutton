package de.fuhagen.sttp.gui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

/**
 * This class extends the {@link JPanel} by enabling the antialiasing
 * for the graphics object of the component.
 * 
 * @author thomas
 *
 */
class AntiAliasJPanel extends JPanel {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 4431166479790658514L;

    /**
     * This method enables the antialiasing feature and calls the paintComponent
     * method of the {@link JPanel}.
     * 
     * @param g
     *      {@link Graphics} object
     */
    @Override
    public void paintComponent(final Graphics g) {

        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        super.paintComponent(g);
    }

}
