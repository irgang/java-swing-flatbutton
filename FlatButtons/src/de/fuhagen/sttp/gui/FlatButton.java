package de.fuhagen.sttp.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

/**
 * This class implements a nice flat button.
 * 
 * @author thomas
 *
 */
public class FlatButton extends JButton {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 6640178507344514197L;

    /**
     * Border color of this button.
     */
    public static final Color BORDER_COLOR     = Color.BLACK;

    /**
     * This constructor builds a new flat button.
     * 
     * @param action
     */
    public FlatButton(Action action) {
        super(action);

        setForeground(BORDER_COLOR);

        setFocusPainted(false);
        setContentAreaFilled(false);

        Border line = new LineBorder(BORDER_COLOR, 3, true);
        Border empty = new EmptyBorder(5, 5, 5, 5);
        Border compound = new CompoundBorder(line, empty);
        setBorder(compound);
    }

    /**
     * This constructor builds a new flat button with the given border color.
     * 
     * @param action
     */
    public FlatButton(Action action, Color borderColor) {
        super(action);

        setForeground(borderColor);

        setFocusPainted(false);
        setContentAreaFilled(false);

        Border line = new LineBorder(borderColor, 3, true);
        Border empty = new EmptyBorder(5, 5, 5, 5);
        Border compound = new CompoundBorder(line, empty);
        setBorder(compound);
    }

    /**
     * Little workaround for disabled buttons.
     */
    @Override
    public void paintComponent(Graphics g) {

        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        boolean enabled = isEnabled();
        setEnabled(true);
        super.paintComponent(g);
        setEnabled(enabled);
    }
}
