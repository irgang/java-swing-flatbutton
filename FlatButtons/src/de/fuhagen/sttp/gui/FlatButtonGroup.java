package de.fuhagen.sttp.gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.Action;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

/**
 * A {@link FlatButtonGroup} is a mode switch build of {@link FlatButton}s.
 * 
 * @author thomas
 *
 */
public class FlatButtonGroup extends AntiAliasJPanel implements ActionListener {

    /**
     * Serial version UID.
     */
    private static final long  serialVersionUID = -2581607936154386661L;

    /**
     * Vector for buttons in group.
     */
    private Vector<FlatButton> buttons;
    /**
     * Current enabled button.
     */
    private FlatButton         enabledButton    = null;
    /**
     * Border for selected button.
     */
    private Border             enabledBorder;
    /**
     * Border for not selected buttons.
     */
    private Border             buttonBorder;

    public FlatButtonGroup() {
        this(Color.BLACK);
    }

    /**
     * This constructor creates a new {@link FlatButtonGroup} with the given border color.
     * 
     * @param groupBorder
     *      border color of group
     */
    public FlatButtonGroup(Color groupBorder) {

        Border line = new LineBorder(Color.BLUE, 3, true);
        Border empty = new EmptyBorder(5, 5, 5, 5);
        Border enabled = new CompoundBorder(line, empty);

        line = new LineBorder(Color.BLACK, 3, true);
        empty = new EmptyBorder(5, 5, 5, 5);
        Border other = new CompoundBorder(line, empty);

        enabledBorder = enabled;
        buttonBorder = other;

        buttons = new Vector<FlatButton>();

        setLayout(new FlowLayout(FlowLayout.CENTER));

        line = new LineBorder(groupBorder, 3, true);
        empty = new EmptyBorder(5, 5, 5, 5);
        Border group = new CompoundBorder(line, empty);
        setBorder(group);
    }

    /**
     * This constructor creates a new button group.
     * 
     * @param enabled
     *      border for selected button
     * @param other
     *      border for other buttons
     * @param groupBorder
     *      border color for group
     */
    public FlatButtonGroup(Border enabled, Border other, Color groupBorder) {
        enabledBorder = enabled;
        buttonBorder = other;

        buttons = new Vector<FlatButton>();

        setLayout(new FlowLayout(FlowLayout.CENTER));

        Border line = new LineBorder(groupBorder, 3, true);
        Border empty = new EmptyBorder(5, 5, 5, 5);
        Border group = new CompoundBorder(line, empty);
        setBorder(group);
    }

    /**
     * This method adds a new button to the group.
     * 
     * @param button
     *      {@link FlatButton} to add
     */
    public void addButton(FlatButton button) {
        buttons.add(button);

        button.addActionListener(this);

        add(button);

        button.setBorder(buttonBorder);

        if (enabledButton == null) {
            enabledButton = button;
            button.setBorder(enabledBorder);
            button.setEnabled(false);
            button.invalidate();
            button.repaint();
        }
    }

    /**
     * This method adds a new {@link FlatButton} for the given {@link Action} to the group.
     * 
     * @param action
     *      button action
     */
    public void addAction(Action action) {
        FlatButton button = new FlatButton(action);
        addButton(button);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (buttons.contains(e.getSource())) {
            FlatButton button = (FlatButton) e.getSource();

            enabledButton.setBorder(buttonBorder);
            enabledButton.setEnabled(true);
            enabledButton.invalidate();
            enabledButton.repaint();

            enabledButton = button;
            button.setBorder(enabledBorder);
            enabledButton.setEnabled(false);
            enabledButton.invalidate();
            enabledButton.repaint();
        }
    }
}
