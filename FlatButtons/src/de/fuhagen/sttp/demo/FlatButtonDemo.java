package de.fuhagen.sttp.demo;

import java.awt.Color;
import java.awt.FlowLayout;
import java.net.URL;

import javax.swing.JFrame;

import de.fuhagen.sttp.gui.FlatButton;
import de.fuhagen.sttp.gui.FlatButtonGroup;

/**
 * Demo of {@link FlatToggleButton} and {@link FlatTrippleButton} GUI elements.
 * 
 * @author thomas
 *
 */
public class FlatButtonDemo extends JFrame {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 683933609536453500L;

    /**
     * This method create a new {@link ZoomSlider} demo frame.
     * 
     * @param args
     */
    public static void main(String[] args) {
        new FlatButtonDemo();
    }

    /**
     * This constructor builds a JFrame with {@link FlatButton}s and a {@link FlatButtonGroup}.
     */
    public FlatButtonDemo() {
        super("Flat button demo");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout(FlowLayout.LEFT));

        URL url = this.getClass().getClassLoader().getResource(
                "de/fuhagen/sttp/resources/node.png");
        DemoAction nodeAction = new DemoAction("node", url);
        url = this.getClass().getClassLoader().getResource(
                "de/fuhagen/sttp/resources/arc.png");
        DemoAction arcAction = new DemoAction("arc", url);
        url = this.getClass().getClassLoader().getResource(
                "de/fuhagen/sttp/resources/move.png");
        DemoAction moveAction = new DemoAction("move", url);
        url = this.getClass().getClassLoader().getResource(
                "de/fuhagen/sttp/resources/delete.png");
        DemoAction deleteAction = new DemoAction("delete", url);

        FlatButton blackButton = new FlatButton(nodeAction, Color.BLACK);
        FlatButton blueButton = new FlatButton(arcAction, Color.BLUE);
        FlatButton greenButton = new FlatButton(moveAction, Color.GREEN);
        FlatButton redButton = new FlatButton(deleteAction, Color.RED);

        add(blackButton);
        add(blueButton);
        add(greenButton);
        add(redButton);

        FlatButtonGroup buttonGroup = new FlatButtonGroup();
        buttonGroup.addAction(nodeAction);
        buttonGroup.addAction(arcAction);
        buttonGroup.addAction(moveAction);
        buttonGroup.addAction(deleteAction);
        add(buttonGroup);

        setSize(400, 250);
        setVisible(true);
    }
}
