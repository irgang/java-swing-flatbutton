package de.fuhagen.sttp.demo;

import java.awt.event.ActionEvent;
import java.net.URL;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;

/**
 * Demo action for {@link FlatButtonDemo}.
 * 
 * @author thomas
 *
 */
public class DemoAction extends AbstractAction {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 1990080725055658590L;
    /**
     * Name of this action.
     */
    private String            name             = "";

    /**
     * This constructor builds a new demo action.
     * 
     * @param actionName
     *      name / short description of action
     */
    public DemoAction(String actionName, URL iconURL) {
        super();

        name = actionName;

        putValue(Action.SHORT_DESCRIPTION, actionName);
        ImageIcon icon = new ImageIcon(iconURL);
        putValue(Action.SMALL_ICON, icon);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(name + " action fired");
    }
}
