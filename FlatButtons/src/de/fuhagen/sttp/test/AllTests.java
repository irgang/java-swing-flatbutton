package de.fuhagen.sttp.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({FlatButtonGroupTest.class, FlatButtonTest.class})
public class AllTests {

}
