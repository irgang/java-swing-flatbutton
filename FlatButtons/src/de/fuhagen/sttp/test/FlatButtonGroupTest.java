package de.fuhagen.sttp.test;

import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.net.URL;

import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import org.junit.Test;

import de.fuhagen.sttp.demo.DemoAction;
import de.fuhagen.sttp.gui.FlatButton;
import de.fuhagen.sttp.gui.FlatButtonGroup;

public class FlatButtonGroupTest {

    private final URL icon = this.getClass().getClassLoader().getResource(
                                   "de/fuhagen/sttp/resources/node.png");

    @Test
    public void testAddButton() {
        DemoAction demo = new DemoAction("demo", icon);
        FlatButton button = new FlatButton(demo);
        FlatButton button2 = new FlatButton(demo);

        Border line = new LineBorder(Color.BLUE, 3, true);
        Border empty = new EmptyBorder(5, 5, 5, 5);
        Border enabled = new CompoundBorder(line, empty);

        line = new LineBorder(Color.BLACK, 3, true);
        empty = new EmptyBorder(5, 5, 5, 5);
        Border disabled = new CompoundBorder(line, empty);

        FlatButtonGroup group = new FlatButtonGroup(enabled, disabled,
                Color.GREEN);

        int count = group.getComponentCount();
        group.addButton(button);
        assertTrue("component count", count + 1 == group.getComponentCount());
        assertTrue("enalbed border", button.getBorder().equals(enabled));

        group.addButton(button2);
        assertTrue("component count", count + 2 == group.getComponentCount());
        assertTrue("disabled border", button2.getBorder().equals(disabled));
    }

    @Test
    public void testAddAction() {
        DemoAction demo = new DemoAction("demo", icon);

        Border line = new LineBorder(Color.BLUE, 3, true);
        Border empty = new EmptyBorder(5, 5, 5, 5);
        Border enabled = new CompoundBorder(line, empty);

        line = new LineBorder(Color.BLACK, 3, true);
        empty = new EmptyBorder(5, 5, 5, 5);
        Border disabled = new CompoundBorder(line, empty);

        FlatButtonGroup group = new FlatButtonGroup(enabled, disabled,
                Color.GREEN);

        int count = group.getComponentCount();
        group.addAction(demo);
        assertTrue("component count", count + 1 == group.getComponentCount());

        group.addAction(demo);
        assertTrue("component count", count + 2 == group.getComponentCount());
    }
}
