package de.fuhagen.sttp.test;

import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.net.URL;

import org.junit.Test;

import de.fuhagen.sttp.demo.DemoAction;
import de.fuhagen.sttp.gui.FlatButton;

public class FlatButtonTest {

    private final URL icon = this.getClass().getClassLoader().getResource(
                                   "de/fuhagen/sttp/resources/node.png");

    @Test
    public void testFlatButtonAction() {
        DemoAction demo = new DemoAction("demo", icon);
        FlatButton button = new FlatButton(demo);

        assertTrue("action", button.getAction() == demo);
    }

    @Test
    public void testFlatButtonAbstractActionColor() {
        DemoAction demo = new DemoAction("demo", icon);
        FlatButton button = new FlatButton(demo, Color.RED);

        assertTrue("action", button.getAction() == demo);
    }
}
